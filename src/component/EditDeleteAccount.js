import React, {useContext} from 'react';
import Layout from "./Layout";
import {AccountContext} from "../App";
import {Link} from "react-router-dom";

function EditDeleteAccount(props) {
    const {users, setUsers} = useContext(AccountContext);
    const deleteHandler = (id) => {
        setUsers(users.filter((user) => user.id !== id));
    };
    const renderUsers = () => {
        return users.map(user => {
            return <tr key={user.id}>
                <td>{user.id}</td>
                <td>{user.name}</td>
                <td>{user.phone}</td>
                <td>{user.email}</td>
                <td>{user.deposit}</td>
                <td>
                    <Link className="btn btn-warning me-3" to={"/editAccount/" + user.id}>Edit</Link>
                    <button className="btn btn-danger" onClick={() => deleteHandler(user.id)}>Delete</button>
                </td>
            </tr>;
        });
    };
    return (
        <Layout title={"EDIT DELETE ACCOUNT"}>
            {users.length > 0 ? <table className="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Deposit</th>
                    <th>Edit - Delete</th>
                </tr>
                </thead>
                <tbody>
                {renderUsers()}
                </tbody>
            </table> : <p>No user exist!</p>}
        </Layout>
    );
}

export default EditDeleteAccount;