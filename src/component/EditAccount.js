import React, {useContext, useEffect, useState} from 'react';
import Layout from "./Layout";
import {useNavigate, useParams} from "react-router-dom";
import {AccountContext} from "../App";

function EditAccount() {
    const {users, setUsers} = useContext(AccountContext);
    const {id} = useParams();
    const redirect = useNavigate();
    const [user, setUser] = useState({
        name: "", phone: "", email: "", deposit: ""
    });
    useEffect(() => {
        setUser(users.find((el) => el.id === parseInt(id)));
    }, []);

    const inputHandler = (e) => {
        setUser({...user, [e.target.name]: e.target.value});
    };

    const submitHandler = (e) => {
        e.preventDefault();
        setUsers(users.map((el) => {
            if (el.id === user.id) {
                return user;
            } else {
                return el;
            }
        }));
        redirect("/editDelete");
    };
    return (
        <Layout title={"EDIT ACCOUNT"}>
            {user.hasOwnProperty("name") && <form onSubmit={submitHandler}>
                <input
                    className="form-control mb-2"
                    type="text"
                    name="name"
                    placeholder="Name"
                    value={user.name}
                    onInput={inputHandler}/>
                <input
                    className="form-control mb-2"
                    type="text"
                    name="phone"
                    placeholder="Phone"
                    value={user.phone}
                    onInput={inputHandler}/>
                <input
                    className="form-control mb-2"
                    type="text"
                    name="email"
                    placeholder="Email"
                    value={user.email}
                    onInput={inputHandler}/>
                <input
                    className="form-control mb-2"
                    type="text"
                    name="deposit"
                    placeholder="Deposit"
                    value={user.deposit}
                    onInput={inputHandler}/>
                <button className="btn btn-primary px-5">SAVE CHANGES</button>
            </form>}
        </Layout>
    );
}

export default EditAccount;