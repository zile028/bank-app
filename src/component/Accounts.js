import React, {useContext, useEffect} from 'react';
import {AccountContext} from "../App";
import Layout from "./Layout";

function Accounts(props) {
    const {users} = useContext(AccountContext);
    const renderUsers = () => {
        return users.map(user => {
            return <tr key={user.id}>
                <td>{user.id}</td>
                <td>{user.name}</td>
                <td>{user.phone}</td>
                <td>{user.email}</td>
                <td>{user.deposit}</td>
            </tr>;
        });
    };
    return (
        <Layout title={"ACCOUNT"}>
            {users.length ? <table className="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Deposit</th>
                </tr>
                </thead>
                <tbody>
                {renderUsers()}
                </tbody>
            </table> : <p>No user exist.</p>}
        </Layout>
    );
}

export default Accounts;