import React, {useContext, useEffect, useState} from 'react';
import Layout from "./Layout";
import {AccountContext} from "../App";
import {useNavigate} from "react-router-dom";

function AddAccount() {
    const {users, setUsers} = useContext(AccountContext);
    const [inputData, setInputData] = useState({
        name: "", phone: "", email: "", deposit: ""
    });
    const redirect = useNavigate();

    const inputHandler = (e) => {
        setInputData({...inputData, [e.target.name]: e.target.value});
    };

    const submitHandler = (e) => {
        e.preventDefault();
        setUsers([...users, {...inputData, id: new Date().getTime()}]);
        redirect("/");
    };
    return (
        <Layout title={"ADD ACCOUNT"}>
            <form onSubmit={submitHandler}>
                <input
                    className="form-control mb-2"
                    type="text"
                    name="name"
                    placeholder="Name"
                    onInput={inputHandler}/>
                <input
                    className="form-control mb-2"
                    type="text"
                    name="phone"
                    placeholder="Phone"
                    onInput={inputHandler}/>
                <input
                    className="form-control mb-2"
                    type="text"
                    name="email"
                    placeholder="Email"
                    onInput={inputHandler}/>
                <input
                    className="form-control mb-2"
                    type="text"
                    name="deposit"
                    placeholder="Deposit"
                    onInput={inputHandler}/>
                <button className="btn btn-primary px-5">ADD</button>
            </form>
        </Layout>
    );
}

export default AddAccount;