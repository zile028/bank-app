import './App.css';
import {Outlet} from "react-router-dom";
import {createContext, useEffect, useState} from "react";
import {usersDb} from "./usersDb";

export const AccountContext = createContext(null);

function App() {
    const [users, setUsers] = useState([]);
    useEffect(() => {
        if (localStorage.hasOwnProperty("users")) {
            setUsers(JSON.parse(localStorage.getItem("users")));
        }
    }, []);

    useEffect(() => {
        if (users.length > 0) {
            localStorage.setItem("users", JSON.stringify(users));
        }
    }, [users]);

    return <>
        <AccountContext.Provider value={{users, setUsers}} children={<Outlet/>}/>
    </>;
}

export default App;
