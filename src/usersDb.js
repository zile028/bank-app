export const usersDb = [
    {
        id: 1,
        name: "Dejan Zivkovic",
        phone: "123-123-123",
        deposit: 4000,
        email: "dejan@mail.com"
    },
    {
        id: 2,
        name: "Marko Petrovic",
        phone: "456-456-456",
        deposit: 3000,
        email: "marko@mail.com"
    }
];